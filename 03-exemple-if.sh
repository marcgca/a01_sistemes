#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Exemple de if
# -------------------------------------

edat=$1
echo $edat
if [ $edat -lt 18 ]
then
  echo "menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat laboral"
else
	echo "jubilat"
fi
