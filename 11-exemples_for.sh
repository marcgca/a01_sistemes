#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Exemples bucle for
# ------------------------------------
ERR_NARGS=1
ERR_NOVALID=2
OK=0
# Validem que hi hagi argument



# Iterar els arguments numerats
cont=0
for arg in $*
do
  cont=$((cont+1))
  echo "$cont: $arg"
done
exit $OK



# Iterar per la llista d'arguments
for arg in "$@" # $*
do
  echo $arg
done
exit $OK

# Itera pels elements resultat d'una ordre mitjançant command subtitution
llista=$(ls)
for nom in $llista
do
 echo $nom
done
exit $OK


# Itera el contingut d'una variable
llista="pere pau marta anna"
for nom in $llista
do
  echo $nom
done
exit $OK
# Iterar per una llista de noms
for nom in "pere" "pau" "marta" "anna"
do
  echo $nom
done

exit $OK
