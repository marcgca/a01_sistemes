#! /bin/bash
# @edt ASIX M01-ASO Curs 2018-2019
# $ llistar-dir-sh dir
#-----------------------------------
# verificar 1 arg, i que és un dir
ERROR_NARG=1
ERROR_NDIR=2
OK=0

# Comprovar que sol hi ha 1 argument
if [ $# -ne 1 ]
then
	echo "Numero d'arguments invàlid"
	echo "Usage: prog.sh dir"
	exit $ERROR_NARG
fi

dir=$1
# comprovar que sigui un direcori
if ! [ -d $dir ]
then
	echo "Argument invàlid, $dir no és un direcori"
	echo "Usage: prog.sh dir"
	exit $ERROR_NDIR
fi

# fa un 'ls' del direcori rebut
ls $dir

exit $OK
