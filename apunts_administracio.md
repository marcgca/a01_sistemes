## Administració d'usuaris/grups/passwords...

### Fitxers de configuració generals

* `/etc/passwd`: Arxiu on es guarden els usuaris amb més dades relacionades
* `/etc/group`: Arxiu on es guarden els grups amb més info relacionada
* `/etc/default/useradd`: Valors per defecte del *useradd*
* `/etc/login.defs`:  Valors per defecte dels logins
* `/etc/shells`: Indica quins són els shells vàlids
* `/etc/skel`: L'esquelet de directoris i fitxers que es farà servir per als usuaris. Es pot crear un esquelet en particular i especificar que es fagi servir aquell.

### Fitxers de configuració/accions d'usuari

* `~/.bashlogout`: Indica les accions que volem que fagi quan l'usuari tanca sessió.
* `~/.bash_profile`:  Carrega les opcions específiques de l'usuari, conté variables d'entorn i startup programs (programes que s'inicien al accedir a la sessió). Si volem que les variables perdurin en subsessions (sessions que obrim des de la sessió) les hem d'exportar dins del propi fitxer.
* `~/.bashrc`: Conté els alies i funcions específiques de **l'usuari**
* `/etc/bashrc`: Conté els alies i funcions per a **tots** els usuaris del sistema.

### Usuaris

Tots els usuaris tenen un UID i un GID (grup principal). De forma opcional poden pertanyer a altres grups secundaris, però sí o sí ha de tindre un grup principal.

Les opcions per defecte de la creació d'un usuari es troben a `/etc/login.defs`

