#! /bin/bash
# @EDT ASIX 2018/2019
# separar en dues llistes args i opcions
#----------------------------------------------------------------------------------------
ERR_NARGS=1
ERR_NOPT=2
OK=0

# 1) Validar n arguments
if [ $# -lt 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: prog.sh [-a, -b, -c, -d, -e] args[...]"
	exit $ERR_NARGS
fi

# 
llistaOpcions=""
llistaArguments=""
llistaFiles=""
llistaNums=""


while [ -n "$1" ]
do
    case $1 in 
    -a)
        llistaOpcions="$llistaOpcions $1"
        llistaFiles="$llistaFiles $2"
        shift;;
    -d)
        llistaOpcions="$llistaOpcions $1"
        llistaNums="$llistaNums $2"
        shift;;
    -b | -c | -e)
        llistaOpcions="$llistaOpcions $1"
        ;;
    *)
        llistaArguments="$llistaArguments $1"
        ;;
    esac
    shift
done

echo "opcions: $llistaOpcions"
echo "arguments: $llistaArguments"
echo "files: $llistaFiles"
echo "nums: $llistaNums"
exit 0
