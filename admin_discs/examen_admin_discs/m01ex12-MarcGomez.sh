# M01 Sistemes Operatius HISX1
# Marc Gomez 
# isx47797439
# Examen 12 02/05/19
# Fes un programa que rep opcionalment com a argument un nom de fitxer i si no el rep processa l'entrada estàndard.
# Per cada fs ens llista el resultat de l'ordre df
# --------------------------------------------------------------------------------------
OK=0
ERR_NARGS=1
ERR_FSINVALID=2

if [ $# -lt 1 ]
then
	echo "ERROR nª args invalid"
	echo "Usage: prog.sh fs[...]"
	exit $ERR_NARGS
elif [ $# -eq 1 -a $1 == "-h" -o $1 =="--help" ]
then
	echo "Usage: prog.sh filesystem[...]"
	exit $OK
fi

filesystems=$*

for fs in $filesystems
do
	#llistem les entrades amb el mateix fs
	llista_fs=$(df -h -t $fs)
	
	# contem quantes entrades té
	cont_fs=$(echo $llista_fs | wc -l)
	# Ho mostrem per cada entrada
	echo "$fs: ($cont_fs)"
	for entrada in $llista_fs
	do
		echo $entrada | tr -s "[:blank:]" ":" | cut -d: -f2- | tr -s ":" "\t"
	done

done
