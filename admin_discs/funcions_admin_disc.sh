# @edt ASIX M01_ISO Curs 2018-2019
# Administració de disc
# Marc Gómez
# isx47797439

#### Exercicis
#
#Fes les següents funcions:
#
#
#1- fsize 
#Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.

function fsize() {
	login=$1
	home_user=$(grep "^$login:" /etc/passwd | cut -d: -f6)
	if [ -z $home_user ]
	then
		echo "ERR $home_user no existeix" >> /dev/stderr
		return 1
	fi
	du -hs $home_user
}

#2- loginargs
#Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize.
#Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no
#generra una traça d'error.

function loginargs() {
	for login in $*
	do
		echo "$login"
		fsize $login
	done

}

#3- loginfile
#Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada
#usuari usant fsize. Verificar que es rep un argument i que és un regular file.

function loginfile(){
	file=$1
	while read -r login 
	do
		fsize $login
	done < $file
}

#4- loginboth
#Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia.
#Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login
#rebut que és vàlid.

function loginboth(){
	if [ $# -gt 1 ];then
		return 1
	elif [ $# -eq 1 ] && ! [ -f $1 ];then
		return 2
	fi

	file=/dev/stdin
	if [ $# -eq 1 ];then
		file=$1
		echo "es file"
	fi
	
	while read -r login 
	do
		fsize $login
	done < $file
}


#5- grepgid
#Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal.
#Verificar que es rep un argument i que és un GID vàlid.

function grepgid(){

	if [ $# -ne 1 ];then
		echo "Error: numero de argumentos incorrecto" >> /dev/stderr
		return 1
	fi 

	gid=$1
	users=$(grep -E "^([^:]*:){3}$gid:" /etc/passwd | cut -d: -f1)
	
	if [ -z "$users" ];then
		echo "Error: gid $gid no existe" >> /dev/stderr
		return 2
	fi

	echo $users 
}

#6- gidsize
#Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home.
#Verificar que es rep un argument i que és un gID vàlid.

function gidsize(){
	if [ $# -ne 1 ];then
		echo "Error: numero de argumentos incorrecto" >> /dev/stderr
		return 1
	fi 
	gid=$1
	users=$(grepgid $gid 2>> /dev/null)
	count=0
	for login in $users
	do
		fsize $login
	done
}

#7- allgidsize 
#Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.

function allgidsize(){
	gidsDeUsuaris=$(cut -d: -f4 /etc/passwd | sort -n | uniq)
	for gid in $gidsDeUsuaris
	do
		echo "Gid: $gid"
		gidsize $gid | sed 's/^/\t/'
	done
}

#8- allgroupsize 
#Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen.
#Ampliar filtrant només els grups del 0 al 100.

function allgroupsize(){
	while read -r line 
	do
		gid=$(echo $line | cut -d: -f3)
		if [ $gid -ge 0 ] && [ $gid -le 100 ];then
			
			homeSize=$(gidsize $gid 2>> /dev/null)
			
			if ! [ -z "$homeSize" ];then
				echo "Gid: $gid"
				echo $homeSize  | sed 's/^/\t/'
			fi
		fi
	done < /etc/group
}


#9-fstype 
#Donat un fstype llista el device i el mountpoint (per odre de device) 
#de les entrades de fstab d'quest fstype.

function fstype(){
	fstype=$1
	grep -v "^#" fstab | tr -s "[:blank:]" " " | grep -E "^([^ ]* ){2}$fstype " | cut -d" " -f1,2 | sort
}

#10- allfstype
#LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

function allfstype(){
	diferentesFs=$(grep -v -e "^#"  -e "^$" fstab | tr -s "[:blank:]" " " | cut -d" " -f3 | sort | uniq)
	for fs in $diferentesFs
	do
		echo $fs
		fstype $fs | sed 's/^/\t/'
	done
}

#11- allfstypeif
#LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
#Es rep un valor numèric d'argument que indica el numéro mínim d'entrades
#d'aquest fstype que hi ha d'haver per sortir al llistat.

function allfstypeif(){
	
	minimo=$1
	diferentesFs=$(grep -v -e "^#"  -e "^$" fstab | tr -s "[:blank:]" " " | cut -d" " -f3 | sort | uniq)
	
	for fs in $diferentesFs
	do
		numRegistros=$(fstype $fs | wc -l)
		if [ $numRegistros -ge $minimo ]; then
			echo $fs
			fstype $fs | sed 's/^/\t/'
		fi
	done
}



