#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Exemples case
# ---------------------------------
dia=$1
case $dia in
  'dilluns' | 'dimarts' | 'dimecres' | 'dijous' | 'divendres')
	 echo "$dia és laborable";;
  'dissabte' | 'diumenge' )
  	echo "$dia és festiu";;
  *)
  	echo "$dia no és un dia";;
esac
exit 0

lletra=$1
case $lletra in
  'a' | 'e' | 'i' | 'o' | 'u')
	  echo "$lletra és una vocal";;
  *)
	  echo "$lletra és una consonant";;
esac

exit 0
