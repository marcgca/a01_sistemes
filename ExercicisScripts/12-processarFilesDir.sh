#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar passwd
# 12) Programa: prog.sh -h uid...
#	Per a cada uid:
#	gname: GNAME, gid: GID, users: USERS
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1



for uid in $*
do
  uLine=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  if [ $? -eq 0 ]
  then
		login=$(echo $uLine | cut -d: -f1)
		gid=$(echo "$uLine" | cut -d: -f4)
		gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
		dirHome=$(echo "$uLine" | cut -d: -f6)
		shell=$(echo $uLine | cut -d: -f7)
		echo "$login($uid) $gname $dirHome $shell"
	else
		echo "Error $uid no és un user vàlid" >> /dev/stderr
	fi
done
exit 0

