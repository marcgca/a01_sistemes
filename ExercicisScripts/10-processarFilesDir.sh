#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar passwd
# 10) Programa: prog.sh
#	Rep per stdin GIDs i llista per stdout la informació de cada un d'aquests grups, en format:
#	gname: GNAME, gid: GID, users: USERS
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1

while read -r gid
do
  groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]
  then
	  gname=$(echo "$groupLine" | cut -d: -f1)
		users=$(echo "$groupLine" | cut -d: -f4)
		echo "gname: $gname, gid: $gid, users: $users"
	else
		echo "Error $gid no és un grup vàlid" >> /dev/stderr
	fi
done
exit 0

