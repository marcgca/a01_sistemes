#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar files/directoris
# 7) Programa: prog -f/-d arg1 arg2 arg3 arg4
#  a) Valida que tots els arguments rebuts són del tipus que indica el flag. És a dir, si es 
#   crida amb -f valida que tots quatre són file. Si es crida amb -d valida valida 
#    que tots quatre són directoris.
#    Retorna 0 ok, 1 error num args, 2 hi ha elements errònis.
#    Exemple: prog -f carta.txt a.txt /tmp fi.txt -> retorna status 2.
#  b) Ampliar amb el cas: prog -h/--help
#		
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
if [ "$#" -ne 5 ]
then
  echo "ERROR numero args"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
if [ "$1" != "-f" -a "$1" != "-d" -a "$1" != "-h" -a "$1" != "--help" ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
tipus=$1
shift
for arg in $*
do
  if ! [ $tipus "$arg" ]; then
    status=2
  fi
done
exit $status

