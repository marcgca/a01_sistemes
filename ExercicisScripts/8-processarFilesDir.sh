#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar files/directoris
# 8) Programa: prog file…
#    a)Validar existeix almenys un argument. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit 
#     correctament, o un missatge d’error per stderror si no s’ha 
#     pogut comprimir. En finalitzar es mostra per stdout quants 
#     files ha comprimit. 
#      Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#    b) Ampliar amb el cas: prog -h|--help.	
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1
if [ "$#" -lt 1 ]
then
  echo "ERROR numero args"
  echo "usage: 08_valida_arg.sh file[...]"
  exit $ERR_NARG
fi
if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "usage: 08_valida_arg.sh file[...]"
  exit $status
fi

for file in $*
do
  tar cvf "$file.tar" $file
  if [ $? -eq 0 ]
  then
    echo "$file.tar" >> /dev/stdout
    compt=$((compt+1))
  else
    echo "$file no comprimit" >> /dev/stderr
    status=2
  fi
done

echo "S'han comprimit $compt fitxers" >> /dev/stdout
exit $status

