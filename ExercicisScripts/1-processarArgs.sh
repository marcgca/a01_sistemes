#! /bin/bash
# @EDT ASIX 2018/2019
# Processar arguments
# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#----------------------------------------------------------------------------------------
OK=0

for arg in $*
do
	echo $arg | egrep ".{4,}"
done

exit 0
