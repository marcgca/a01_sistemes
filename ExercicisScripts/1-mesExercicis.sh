#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar passwd
# 1) Dir si és jove, actiu, jubilat
#	processar n edats rebudes com arguments
#	
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1

if [ $# -eq 0 ]
then
	echo "Error: no hi han arguments"
	echo "Usage; prog.sh edat1..."
	exit $ERR_NARG
fi

for edat in $*
do	
	echo $edat | egrep "^[0-9]{1,3}$" >> /dev/null
	if [ $? -eq 0 ]
	then
		if [ $edat -lt 18 ]
		then
			echo "$edat és jove"
		elif [ $edat -lt 65 ]
		then
			echo "$edat és actiu"
		else
			echo "$edat és jubilat(de moment)"
		fi
	else
		echo "$edat no és una edat vàlida"
	fi
done


exit 0

