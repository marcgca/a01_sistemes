#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar files/directoris
# 8)Programa: prog.sh [ -r -m -c cognom -j -e edat ] arg...
# Escriure el codi que desa en les variables: ​ opcions ​ , ​ cognom ​ , ​ edat i ​ arguments els valors
# corresponents.
# No cal validar ni mostrar res!
# Per exemple si es crida: ​ $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
# retona: opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
#--------------------------------------------------------------------------------------------------------------------------


llistaOpcions=""
cognom=""
edat=""
arguments=""

while [ -n "$1" ]
do
    case $1 in
    -r | -j)
        llistaOpcions="$llistaOpcions $1";;
    -e)
        edat="$2"
        shift;;
    -c)
        cognom="$2"
        shift;;
    *)
        arguments="$arguments $1"
        ;;
    esac
    shift
done

echo "Opcions: $llistaOpcions"
echo "Edat: $edat"
echo "Cognom: $cognom"
echo "Arguments: $arguments"
exit 0

