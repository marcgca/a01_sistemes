#! /bin/bash
# @EDT ASIX 2018/2019
# Processar arguments
# 3) Processar els arguments que són matricules
#		a) llistar les vàlides, del tipus: 9999 AAA
#		b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d'errors (de no vàlides)
#--------------------------------------------------------------------------------------------------------------------------
OK=0

num=0
for arg in "$@"
do
	echo $arg | egrep "^[0-9]{4} [a-Z]{3}$" 2> /dev/null
	if [ $? -ne 0 ]
	then
		echo "$arg es no vàlid" >> /dev/stderr
		num=$((num+1))
	fi
done


exit $num
