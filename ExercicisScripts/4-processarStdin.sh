#! /bin/bash
# @EDT ASIX 2018/2019
# Processar stdin
# 4) Processar stdin mostrant per stdout les línies numerades i en majúscules
#		
#--------------------------------------------------------------------------------------------------------------------------
OK=0

num=1
while read -r line
do
	echo "$num: $line" | tr "[a-z]" "[A-Z]"
	num=$((num+1))	
done

exit $OK
