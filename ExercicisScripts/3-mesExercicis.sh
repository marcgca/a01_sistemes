#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar passwd
# 3) processar ganmes rebuts per 
#    arguments i mostrar per a cada un el llistat
#	 dels usuaris del passwd que pertanyen a grup.
#	
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1

for gname in $*
do
#	echo "$gname"
	gid=$(egrep "^$gname:" /etc/group | cut -d: -f3)
#	echo "$gid"
	llistaLogins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd)
#	echo "$llistaLogins"
	for user in $llistaLogins
	do
		user=$(echo "$user" | cut -d: -f1)
		login=$(egrep "^$user:" /etc/passwd | cut -d: -f1)
		echo "$login"
	done
done


exit 0

