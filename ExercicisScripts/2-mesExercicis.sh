#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar passwd
# 2) processar stdin login i mostrar login, uid i gid de cada un.
#	
#--------------------------------------------------------------------------------------------------------------------------
#Validar args
status=0
compt=0
ERR_NARG=1

while read -r login
do
	uid=$(egrep "^$login:" /etc/passwd | cut -d: -f3)
	gid=$(egrep "^$login:" /etc/passwd | cut -d: -f4)
	echo "$login, $uid, $gid"
done


exit 0

