#! /bin/bash
# @EDT ASIX 2018/2019
# Processar arguments
# 2) Processar els arguments i comptar quantes n'hi ha de de 3 o més caràcters
#----------------------------------------------------------------------------------------
OK=0

num=0
for arg in $*
do
	echo $arg | egrep ".{3,}" &> /dev/null
	if [ $? -eq 0 ]
	then
		num=$((num+1))
	fi
done

echo "Hi han $num arguments que tenen 3 o més caràcters"
exit $OK
