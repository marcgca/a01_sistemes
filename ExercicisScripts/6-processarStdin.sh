#! /bin/bash
# @EDT ASIX 2018/2019
# Marc Gómez isx47797439
# Processar stdin
# 5) Processar per stdin línies d'entrada tipus "Tom Snyder" i mostrar per stdout la línia en format -> T. Snyder
#		
#--------------------------------------------------------------------------------------------------------------------------
OK=0
while read -r line
do
	echo $line | sed -r 's/^(.)[^ ]* /\1. /g'
done

exit $OK
