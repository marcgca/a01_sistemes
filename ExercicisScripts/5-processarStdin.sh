#! /bin/bash
# @EDT ASIX 2018/2019
# Processar stdin
# 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters
#		
#--------------------------------------------------------------------------------------------------------------------------
OK=0


while read -r line
do
	echo $line | egrep -v ".{50}"
done

exit $OK
