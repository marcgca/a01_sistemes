#! /bin/bash
# @EDT ASIX 2018/2019
# 
# ---------------------------------------------------------
status=0

# per defecte tenim stdin, però si hi ha un fitxer com argument el seleccionem
file=/dev/stdin
if [ $# -eq 1 ]; then
  file=$1
fi

while read -r line
do
  gid=$(echo $line | cut -d: -f4)
  if [ $gid -ge 500 ]; then
    echo $line | cut -d: -f1,3,4,7
  fi
done < $file
exit 0

# arguments per entrada estàndard
while read -r line
do
  gid=$(echo $line | cut -d: -f4)
  if [ $gid -ge 500 ]; then
    echo $line | cut -d: -f1,3,4,7
  fi
done < $1

exit 0

cont=1
while read -r line
do
  echo "$cont $line"
  cont=$((cont+1)) 
done < $1

exit $status
