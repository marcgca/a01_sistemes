#! /bin/bash
# @EDT ASIX 2018/2019
# Processar arguments
# Cercar un usuari entrat per argument i, si existeix:
# fer un tar del mailbox i del home de l'user a /tmp
# llistar els files de l'usuari al sistema
# llistar els processos de l'usuari
# treballs d'impressió de l'usuari (lp)
#--------------------------------------------------------------------------------------------------------------------------
ERR_NARGS=1
ERR_NUSER=2
OK=0

if ! [ $# -eq 1 ]; then
  do
   echo "ERROR num args incorrecte"
   echo "usage: prog.sh usuari"
   exit $ERR_NARGS
  done

user=$1
dataUser=""
dataUser=$(grep "^$user:" /etc/passwd 2> /dev/null)

if [ -z "$dataUser" ]; then
  do
    echo "ERROR: usuari no vàlid o inexistent"
    exit $ERR_NUSER
  else
    uid=$(echo $dataUser | cut -d: -f3) 
    homeDir=$(echo $dataUser | cut -d: -f6)
    # Esborrem l'user
    echo userdel $user
    # Copiem el home de l'usuari i la bústia de correu
    tar -cvzif /tmp/prova/copia_home_$user.tar.gz $homeDir /var/spool/mail/$user
    echo rm -rf $homeDir
    echo rm /var/spool/mail/$useri
    # Llistar els files de l'usuari al sistema
    echo find / -user $user -print 2> /dev/null
    #find / -uid $uid -print 2> /dev/null
    # Llistar els processos de l'usuari
    ps -u $user
    # Llistar els treballs d'impressió
    lpq -U $login
    lprm -U $login

    # at i cron
    atq -q $login
    crontab -u $login -l
    crontab -u $login -r
  done

exit $OK

