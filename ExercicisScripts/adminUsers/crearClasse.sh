#! /bin/bash
# @EDT ASIX 2018/2019
# Programa que rep com a argument el nom de un curs/classe, 
# per exemple hisx1 o carnicers3. Ha de crear el grup 
# (no ha d'existir), el seu home base (/home/nom-del-curs) 
# i 30 usuaris amb noms tipus hisx1-01..hisx1-30.
# ---------------------------------------------------------
status=0
ERR_NARGS=1
ERR_GROUP=2
ERR_DIREXIST=3

if [ $# -ne 1 ]; then
    echo "Error: num args invalid"
    echo "Usage: prog.sh grup"
    exit $ERR_NARGS
fi

grup=$1

grep "^$grup:" /etc/group &> /dev/null
if [ $? -eq 0 ]; then
    echo "Error: el grup $grup ja existeix"
    exit $ERR_GROUP
fi

# Crea el grup
groupadd $grup && echo "Group $grup created OK"

# Crea un home base per als usuaris d'aquest grup
homeUsers="/home/$grup"
mkdir $homeUsers &> /dev/null && echo "directori $grup creat OK"
if [ $? -ne 0 ]; then
  echo "Err directori /home/$grup ja existeix"
  exit $ERR_DIREXIST
fi

# Li donem permisos al home del grup per al propi grup
chgrp $grup $homeUsers $$ echo "permisos canviats OK"


# Crea els usuaris especificats
users=$(echo $grup-{01..30})
for user in $users
do
    useradd -n -b $homeUsers -g $grup $user &> /dev/null && echo "user $user creat OK"
    if [ $? -ne ]; then
      echo "Err $user exist" >> /dev/stder
      status=4
    fi
done


exit $status
