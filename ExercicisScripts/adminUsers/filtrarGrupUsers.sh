#! /bin/bash
# @EDT ASIX 2018/2019
# Llistar els grups que tenen usuaris que hi pertanyen com a grup principal
# -------------------------------------------------------------------------
status=0

# per defecte tenim stdin, però si hi ha un fitxer com argument el seleccionem
file=/dev/stdin
if [ $# -eq 1 ]; then
  file=$1
fi

while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  gname=$(echo $line | cut -d: -f1)
  num=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd &> /dev/null | wc -l)
  if [ $num -ge 3 ]; then
    echo $gname
    egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1,3 | sed -r 's/^(.*)$/\t\1/g'
  fi
done < $file
exit 0
