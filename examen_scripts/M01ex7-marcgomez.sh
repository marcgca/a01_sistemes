#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
#	isx47797439
# Marc Gómez
# Exercici pràctic del examen 7
#-----------------------------
OK=0
ERR_NARGS=1
ERR_EXIST=2
# Verificacions
if [ $# -eq 1 -a $1 = "-h" -o $1 = "--help" ]
then
	echo "@EDT ASIX-M01 Marc Gómez"
	echo "usage: prog.sh fileDesti numPrimer[...]"
	exit $OK
elif [ $# -lt 2 ]
then
	echo "Num arguments incorrecte"
	echo "usage: prog.sh fileDesti numPrimer[...]"
	exit $ERR_NARGS
fi
if [ -e $1 ]
then
	echo "Error: el fitxer destí no pot existir"
	exit $ERR_EXIST
fi
# Programa
fileDesti=$1
shift
processats=0
primers=0
noPrimers=0
for num in $*
do
	processats=$((processats+1))
	bash sophieGermain.sh $num >> /dev/null
	if [ $? -eq 0 ]
	then
		echo "$num"
		echo "$num" >> $fileDesti
		primers=$((primers+1))
	else
		echo "Error: $num no és Sophie Germain primer" >> /dev/stderr
		noPrimers=$((noPrimers+1))
	fi
done
echo "$processats nums processats"
echo "$primers nums primers"
echo "$noPrimers nums no primers"
exit $OK
