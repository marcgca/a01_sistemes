#! /bin/bash
# @EDT ASIX 2018/2019
# Programa que valida si els arguments són correctes i pertanyen a un cas o a un altre
#----------------------------------------------------------------------------------------
ERR_NARGS=1
ERR_NOPT=2
OK=0

# 1) Validar n arguments
if ! [ $# -ge 1 -a $# -le 3 ]
then
	echo "Error: num args incorrecte"
	echo "usage: prog.sh -h / -a arg / -b -a arg"
	exit $ERR_NARGS
fi

# Ajuda
if [ $# -eq 1 ]
then
	if [ "$1" = "-h" -o "$1" = "--help" ]
	then
		echo "@EDT ASIX-M01 Curs 2018/2019"
		echo "Usage: prog.sh -h / -a arg / -b -a arg"
		exit $OK
	else
		echo "Error: num args incorrecte"
		echo "Usage: prog.sh -h / -a arg / -b -a arg"
		exit $ERR_NARGS
	fi
fi

# Validar cas 1
if [ $# -eq 2 ]
then
	if ! [ "$1" = "-a" ]
	then
		echo "Error: cas1 arg invalid"
		echo "usage: prog.sh -a arg"
		exit $ERR_NOPT
	fi
fi

# Validar cas 2
if [ $# -eq 3 ]
then
	if ! [ "$1" = "-b" -a "$2" = "-a" ]
	then
		echo "Error: cas2 arg invalid"
		echo "Usage: prog.sh -b -a arg"
		exit $ERR_NOPT
	fi
fi

# programa
if [ $# -eq 2 ]
then
	echo "cas 1, $2"
else
	echo "cas 2, $3"
fi

exit 0
