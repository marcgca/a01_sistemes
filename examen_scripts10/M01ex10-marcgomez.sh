#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Marc Gómez
# isx47797439
# Examen 10 
# ---------------------------------
OK=0

# Si hi ha un argument l'agafem com mínim d'usuaris
if [ $# -eq 1 ]; then			
	minimUsers=$1
else
	minimUsers=5
fi

# Agafem els grups principals que tenen usuaris
llistaGroups=$(cut -d: -f4 /etc/passwd | sort -n | uniq)

# Iterem per cada grup
for gid in $llistaGroups
do
	# Si el grup té com a mínim els usuaris especificats el mostrem amb el format dessitjat
	numUsers=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
	if [ $numUsers -ge $minimUsers ]; then
		gname=$(egrep "^[^:]*:[^:]*:$gid" /etc/group | cut -d: -f1)
		echo "$gid/$gname:$numUsers"
		egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1,3,6 | sed -r 's/^(.*):(.*):(.*)$/\t\1  \2  \3/g'
	fi
done

exit $OK
