#!/bin/bash
# showuser(login)
# Mostrar els venedors, amb les seves comandes, de l'oficina de Nova York
# amb el següent format:
# num_empleat nom
#           Comanda
# -------------------------------------------------------


function showComandes() {
    oficina=$(egrep "New York" ./oficinas.dat | tr "\t" " " | cut -d" " -f1)
    llistaEmpl=$(sed -r 's/\t/:/g' repventas.dat  | egrep "^[^:]*:[^:]*:[^:]*:$oficina:" | sort -n -t: -k1,1)
    echo $llistaEmpl
    for empleat in $llistaEmpl
    do
        #echo $empleat
        numEmpl=$(echo $empleat | cut -d: -f1) &> /dev/null
        nomEmpl=$(echo $empleat | cut -d: -f2) &> /dev/null
        #echo "$numEmpl $nomEmpl"
    done
}

function showAllGroups() {
  llista_groups=$(cut -d: -f4 /etc/passwd | sort -n | uniq)
  for gid in $llista_groups
  do
    group=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    usuaris=$(egrep "^[^:]*:[^:]*:[^:]:$gid:" /etc/passwd | wc -l)
    if [ $usuaris -ge 2 ]; then
    	echo "$gid ($group)"
    	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 | sort -t: -k1 | sed -r 's/^(.*)$/\t\1/g'
    fi
  done



}