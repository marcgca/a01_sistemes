#!/bin/bash
# showuser(login)
# mostrar un a un els camps amb label

# Per cada login donat mostrar els camps del shadow amb una capçalera
function showShadow() {
  for login in $*
  do
    echo "Login"
    egrep "^$login:" /etc/shadow | cut -d: -f1 | sed -r 's/^(.*)$/\t\1/g'
    echo "Password encriptat"
    egrep "^$login:" /etc/shadow | cut -d: -f2 | sed -r 's/^(.*)$/\t\1/g'
    echo "Última modificació de la contrasenya"
    egrep "^$login:" /etc/shadow | cut -d: -f3 | sed -r 's/^(.*)$/\t\1/g'
    echo "Dies fins que es pugui modificar la contrasenya"
    egrep "^$login:" /etc/shadow | cut -d: -f4 | sed -r 's/^(.*)$/\t\1/g'
    echo "Màxim de temps que l'usuari haurà de modificar la contrasenya"
    egrep "^$login:" /etc/shadow | cut -d: -f5 | sed -r 's/^(.*)$/\t\1/g'
    echo "Període de dies que l'usuari serà avisat de l'expiració de la contrasenya"
    egrep "^$login:" /etc/shadow | cut -d: -f6 | sed -r 's/^(.*)$/\t\1/g'
    echo "Temps que la contrasenya serà vàlida tot i que hagi expirat"
    egrep "^$login:" /etc/shadow | cut -d: -f7 | sed -r 's/^(.*)$/\t\1/g'
    echo "Data d'expiració del compte d'usuari"
    egrep "^$login:" /etc/shadow | cut -d: -f8 | sed -r 's/^(.*)$/\t\1/g'
  done


}



function showAllGroups() {
  llista_groups=$(cut -d: -f4 /etc/passwd | sort -n | uniq)
  for gid in $llista_groups
  do
    group=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    usuaris=$(egrep "^[^:]*:[^:]*:[^:]:$gid:" /etc/passwd | wc -l)
    if [ $usuaris -ge 2 ]; then
    	echo "$gid ($group)"
    	egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 | sort -t: -k1 | sed -r 's/^(.*)$/\t\1/g'
    fi
  done
}

function showAllShells(){
  llista_shells=$(cut -d: -f7 /etc/passwd | sort | uniq)
  for shell in $llista_shells
  do
    usuaris=$(egrep ":$shell$" /etc/passwd | wc -l)  
    if [ $usuaris -ge 2 ]; then
      echo "$shell: $usuaris"
      egrep ":$shell$" /etc/passwd | cut -d: -f1,3,4 | sort -t: -k2n,2 | sed -r 's/^(.*)$/\t\1/g'
    fi 
  done
}


function showGroupMainMembers(){
  ERR_NARGS=1
  ERR_NOGNAME=2
  Ok=0
  #1) validar arg
  if [ $# -ne 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 gname"
    return $ERR_NARGS
  fi
 #2) validar existeix
 gname=$1
 gid=""
 gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
 if [ -z "$gid" ]; then
   echo "Error: login $gname inexist"
   echo "usage: $0 gname"
   return $ERR_NOGNAME
 fi
 grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd \
   | cut -d: -f1,3,7 | sort -t: -k2n,2 | sed 's/:/  /g' | tr '[a-z]' '[A-Z]'
 return $Ok
}



function showUserIn(){
  status=0
  while read -r login
  do
    line=$(grep "^$login:" /etc/passwd)
    if [ $? -ne 0 ]; then
      echo "Err: login $login inei" >> /dev/stderr
      status=$((status+1))
    else
      uid=$(echo $line | cut -d: -f3)
      gid=$(echo $line | cut -d: -f4)
      gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
      gecos=$(echo $line | cut -d: -f5)
      home=$(echo $line | cut -d: -f6)
      shell=$(echo $line | cut -d: -f7)
      echo "$login $uid $gname($gid) $home $shell"
    fi
  done
  return $status
}


function showUserList(){
 ERR_NARGS=1
 status=0
 #1) validar arg
 if [ $# -lt 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login[...]"
   return $ERR_NARGS
 fi
 login_list=$*
 for login in $login_list
 do
  line=$(grep "^$login:" /etc/passwd)
  if [ $? -ne 0 ]; then
   echo "Err: login $login inei" >> /dev/stderr
   status=$((status+1))
  else
   uid=$(echo $line | cut -d: -f3)
   gid=$(echo $line | cut -d: -f4)
   gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
         /etc/group | cut -d: -f1)
   gecos=$(echo $line | cut -d: -f5)
   home=$(echo $line | cut -d: -f6)
   shell=$(echo $line | cut -d: -f7)
   echo "login: $login"
   echo "uid: $uid"
   echo "$gname($gid)"
   echo "home: $home"
   echo "shell: $shell"
  fi
 done
 return $status
}

function showUser(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 uid=$(echo $line | cut -d: -f3)
 gid=$(echo $line | cut -d: -f4)
 gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
	 /etc/group | cut -d: -f1)
 gecos=$(echo $line | cut -d: -f5)
 home=$(echo $line | cut -d: -f6)
 shell=$(echo $line | cut -d: -f7)
 echo "login: $login"
 echo "uid: $uid"
 echo "$gname($gid)"
 echo "home: $home"
 echo "shell: $shell"
 return 0
}

