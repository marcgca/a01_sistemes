#!/bin/bash
# showuser(login)
# mostrar un a un els camps amb label

function showGroupMainMembers(){
 ERR_NARGS=1
 ERR_NGRUP=2
 status=0
 #1) validar arg
 if [$# -ne 1 ]; then
  echo "Error: num args incorrecte"
  echo "usage: $0 gname"
  return $ERR_NARGS
 fi
 #2) Busquem el grup
 grep "^$1:" /etc/group &> /dev/null
 if [ $? -eq 0 ]; then
  gid=$(egrep "^$1:" /etc/group | cut -d: -f3)
  llistaUsers=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd)
  for user in $llistaUsers
  do
   login=$(echo $user | cut -d: -f1)
   uid=$(echo $user | cut -d: -f3)
   dir=$(echo $user | cut -d: f6)
   shell=$(echo $user | cut -d: f7)
   echo "$login $uid $dir $shell"
  done
 else
  echo "Error $0 no és vàlid/no existeix"
  echo "usage: $0 gname"
  return $ERR_NGRUP
 fi
 return $status

}

function showUserIn(){
 status=0
 while read -r login
 do
  line=$(grep "^$login:" /etc/passwd)
  if [ $? -ne 0 ]; then
   echo "Err: login $login inei" >> /dev/stderr
   status=$((status+1))
  else
   uid=$(echo $line | cut -d: -f3)
   gid=$(echo $line | cut -d: -f4)
   gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
   gecos=$(echo $line | cut -d: -f5)
   home=$(echo $line | cut -d: -f6)
   shell=$(echo $line | cut -d: -f7)
   echo "login: $login"
   echo "uid: $uid"
   echo "$gname($gid)"
   echo "home: $home"
   echo "shell: $shell"
  fi
 done
 return $status
}

function showUserList(){
 ERR_NARGS=1
 status=0
 #1) validar arg
 if [ $# -lt 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login[...]"
   return $ERR_NARGS
 fi
 login_list=$*
 for login in $login_list
 do
  line=$(grep "^$login:" /etc/passwd)
  if [ $? -ne 0 ]; then
   echo "Err: login $login inei" >> /dev/stderr
   status=$((status+1))
  else
   uid=$(echo $line | cut -d: -f3)
   gid=$(echo $line | cut -d: -f4)
   gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
         /etc/group | cut -d: -f1)
   gecos=$(echo $line | cut -d: -f5)
   home=$(echo $line | cut -d: -f6)
   shell=$(echo $line | cut -d: -f7)
   echo "login: $login"
   echo "uid: $uid"
   echo "$gname($gid)"
   echo "home: $home"
   echo "shell: $shell"
  fi
 done
 return $status
}

function showUser(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 uid=$(echo $line | cut -d: -f3)
 gid=$(echo $line | cut -d: -f4)
 gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
	 /etc/group | cut -d: -f1)
 gecos=$(echo $line | cut -d: -f5)
 home=$(echo $line | cut -d: -f6)
 shell=$(echo $line | cut -d: -f7)
 echo "login: $login"
 echo "uid: $uid"
 echo "$gname($gid)"
 echo "home: $home"
 echo "shell: $shell"
 return 0
}

