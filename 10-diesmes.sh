#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Rep un mes i retorna els dies que té
# ------------------------------------
ERR_NARGS=1
ERR_NOVALID=2
OK=0
# Validem que hi hagi argument
if [ $# -ne 1 ]
then
    echo "ERROR: num args incorrecte"
    echo "usage: diesmes.sh mes"
    exit $ERR_NARGS
fi

# Afegim l'ajuda
if [ "$1" = "--help" -o "$1" = "-h" ]
then
  echo "@edt ASIX-M01"
  echo "Usage: diesmes.sh mes"
  exit $OK
fi

# Validem que sigui un valor vàlid
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
    echo "ERROR: mes no valid"
    echo "Valors vàlids: 1-12"
    exit $ERR_NOVALID
fi

# Programa
mes=$1
case $mes in
    1 | 3 | 5 | 7 | 8 | 10 | 12)
        echo "31 dies";;
    4 | 6 | 9 | 11)
        echo "30 dies";;
    2)
        echo "28 dies";;
esac
exit $OK
