#! /bin/bash
# @ edt ASIX-M01 Curs 2018-2019
# Validar si l'argument és o no directori
# -------------------------------
# si num args no es correcte plegar
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog dir"
  exit $ERR_NARGS
fi

# Si demana ajuda
if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "@edt ASIX-M01 Marc G"
  echo "usage: esdirectori.sh argument"
  exit 0
fi

# Xixa
if [ -d $1 ]
then
  echo "$1 es un directori"
else
  echo "$1 no és un directori"
fi
exit 0
