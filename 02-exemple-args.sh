#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Exemples d'arguments
# ---------------------------------
echo '$#' $# # El número d'arguments introduïts
echo '$*' $* # La llista dels arguments rebuts
echo '$@' $@ # La llista dels arguments rebuts
echo '$0' $0 # El nom del propi programa
echo '$1' $1 # El primer argument
echo '$2' $2 # El segón argument
echo '$3' $3 # El tercer argument
echo '$4' $4 # El tercer argument
echo '$5' $5 # El tercer argument
echo '$6' $6 # El tercer argument
echo '$7' $7 # El tercer argument
echo '$8' $8 # El tercer argument
echo '$9' $9 # El tercer argument
echo '$10' ${10} # El tercer argument
echo '$11' ${11} # El tercer argument
echo '$12' ${12} # El tercer argument

exit 0
