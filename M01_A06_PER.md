# Patrons Expressió Regular (PER)

- A la documentació ens podem trobar que també s'anomena `regex`

```bash
$ grep "PER" arxiu # Cerca en a l'arxiu el patró especificat

^   # Principi de la línia/paraula
^a # Qualsevol paraula que comenci per "a"

$   # Final de la la línia/paraula
a$ # Qualsevol paraula que acabi per "a"

[]  # Llista que contingui varis arguments, rangs
[aei] # Qualsevol paraula que contingui o "a" o "e" o "i"
[0-9] # Qualsevol paraula que contingui 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

[^] # Llista que NO contingui
[^a] # Llista que NO conté "a"

.   # UN (1) caracter qualsevol

*   # De 0 a n cops el caracter anterior
a*  # De 0 a n cops "a"
.*  # De 0 a n cops qualsevol caracter

{}  # Número de repeticions d'un patró en particular
{n} -> {2} # Que es repeteixi 2 cops
{n,m} -> {2,4} # Mínim i màxim, un mínim de 2 cops i un màxim de 4.
{n,} -> {2,} # Que es repeteixi un mínim de 2 cops fins un màxim indefinit.
{,m} -> {,4} # Que es repeteixi un màxim de 4 cops.

? # El caracter anterior es repeteix cero o un cop
a? # Que o "a" aparegui un cop o no aparegui.

+ # Repetició del caracter anterior un o més cops
a+ # El caracter "a" apareix un (1) o més cops, però apareix sí o sí.
```
```bash
[:alnum:], [:alpha:], [:cntrl:], [:digit:], [:graph:],
[:lower:], [:print:], [:punct:], [:space:], [:upper:],
and [:xdigit:]. For example, [[:alnum:]]
```
```bash
$ echo "hola hole holi" | grep "hol[^ae]"
# Cadena "hol" i qualsevol caracter que no sigui ni "a" ni "e"
holi

$ echo "hola hole holi holiesto" | grep "hol[^ae]."
# Cadena "hol", qualsevol caracter que no sigui ni "a" ni
# "e" i que tingui 1 caracter després
holi holie

$ echo "hola hole holi holiesto" | grep  "hol[^ae]*"
# Cadena "hol" i qualsevol caracter que no sigui ni "a"
# ni "e" de 0 a n cops
hol hol holi holi

$ echo "hola hole holi holiesto" | grep  ".*holi"
# Que tingui qualsevol caracter de 0 a n cops abans 
# d'"holi"
hola hole holi holi

```
- El `*` busca el resultat del patró lo més a la dreta possible, es a dir comença a buscar des del final.

```bash
$ echo "aa:cc:bb:cc:dd" | grep ".*:cc:"
# Qualsevol caracter de 0 a n cops abans de la cadena
# ":cc:"
aa:cc:bb:cc:
```
## GREP
- `Grep` busca coincidencies en un fitxer/cadena amb un patró especificat

### Opcions més utilitzades
- `-i` Ignore case: Ignora el case sensitive, és a dir majúscules/minúscules
- `-v` Invert match: Inverteix la cerca especificada, mostrant les línies que no concorden.
- `-n` Line Number: Mostra el número de la línia al començament d'aquesta.
- `-E` : Acepta expressions regulars tals com `{}`, `|`, `[]`... Sense necessitats d'escaparles amb `\`. També es pot fer servir `egrep` (recomenable).

```bash
# Cerquem el 3er camp que tingui un 1
egrep "^[^:]*:[^:]*:1:" /etc/passwd
daemon:x:1:
```
- Informació del fitxer `/etc/passwd`:
  * `marc`:`x`:`1000`:`1000`:`marc,,,`:`/home/marc`:`/bin/bash`
  * **login**:**contrasenya**:**uid**:**gid**:**gecos**:**home**:**shell**
  * `gecos`: Conté informació sobre l'usuari, com el nom real, telèfon...

## CUT
- `Cut` retalla caracters, columnes (per delimitadors)... I retorna el valor que hem especificat.

```bash
$ head -n1 /etc/passwd
root:x:0:0:root:/root:/bin/bash

# Filtrem per caracters de les files
$ cut -c1-10,12,40- /etc/passwd
root:x:0:0r

#Filtrem per columnes amb un delimitador ":" i 
# seleccionem les columnes
$ cut -d: -f1-3,5,7
root:x:0:root:/bin/bash
```
### Opcions:
- `c` characters: Filtra per caracters.
- `d` delimiter: Especifica el delimitador pel qual filtrarà.
- `f` fields: Especifica els camps pel qual filtrarà, s'utilitza amb `d` per especificar els camps i els seus delimitadors.

## TR
- `Tr` substitueix caracters, ja siguin lletres, espais en blanc, tabuladors...
- La seva sintaxi és -> tr `"c" "a"`
- En el primer bloc `"c"` indiquem el que volem canviar pel segon bloc `"a"`.
  
```bash
# Substituirem els caracters "m,a,r" per "c,e,l" respectivament.
$ echo "la mar estaba salada, merda" | tr "mar" "cel"
le cel estebe selede, celde
```
- En l'exemple sembla que no ha sortit bé, però ho ha executat tal com li hem demanat, ja que ha substituit per cada caracter.
- Tr substitueix caracter a caracter, per substituir paraules és millor fer servir `sed`.

## Blank
- `blank` són els espais en blanc/tabulacions que ens trobem a algun arxius, com `/etc/fstab`. Aquests espais no ens permeten obtenir els camps que volem, per això els hem de normalitzar.

```bash
# Mostrem totes les línies que no comencin per "#"
$ grep -v "^#" /etc/fstab
/dev/sda5         /                ext4 defaults        1   1
/dev/sda7 swap                    swap    defaults        0 0

# Normalitzem els espais per poguer accedir a les columnes
$ grep -v "^#" /etc/fstab | tr -s "[:blank:]" " "
/dev/sda5 / ext4 defaults 1 1
/dev/sda7 swap swap defaults 0 0

```
- `-s` --squeeze-repeats: Repeteix la substitució
  
## SORT
- `sort` ordena les linies d'un fitxer de text/sortida per ordre numeric/alfabètic

```bash
# Ordenem pel 3er camp amb ordre numeric
sort -n -t: -k3,3 passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
```
### Opcions
- `-n` numeric: ordena per valors numerics
- `-t` delimitador: indica el delimitador per a cada camp
- `-k` key: ordena segons el camp que li indiquem
  - `-k3`: Ordena des del 3er camp fins el final de la línia
  - `-k3,3`: Específicament només ordena pel camp 3
  - `-k3n,3`: Específicament només ordena numericament pel camp 3
- `-r` reverse: Ordena al revés

- En el cas de que els valors d'un camp es repeteixin, podem seguir ordenant per un altre camp com és el següent cas:
  ```bash
  ll / | head | tr -s "[:blank:]" ' ' | cut -d" " -f2,3,5,6 | sort -t' ' -k3n,3 -k1n,1

  18 root 3,3K ene
  2 root 4,0K ene
  2 root 4,0K ene
  3 root 4,0K ene
  5 root 4,0K ene
  19 root 4,0K ene
  162 root 12K ene
  1 root 30 ene
  1 root 30 ene

  ```
  - Amb el `tr` normalitzem la sortida a un sol espai.
  - Amb el `cut` agafem les columnes que ens interessen, en aquest cas la 2na, 3era, 5na i 6na.
  - Amb el `sort` ordenem per la 3era columna i, si els resultats són iguals, ordena per la 1era.

  ## SED
  - `Sed` cerca cadenes de caracters que substitueix per una altre cadena especificada o per res.
  - La seva sintaxi és `'s/"patró cerca"/"per a què ho substitueix"/"numero de cops"`

  ```bash
  # Substituim la cadena "mama" per la cadena "papa"
  $ echo "hola mama que tal" | sed 's/mama/papa/'
  hola papa que tal
  ```
  - Podem especificar des d'on pot començar a buscar i substituir:
  
  ```bash
  # substituim un sol cop
  sed 's/mama/papa/'

  # substituim recursivament
  sed 's/mama/papa/g'

  # des de la línia 3 fins el final
  sed '3,$ s/bin/BIN/g' text

  # des de la línia 1 fins la 7 inclosa
  sed '1,7 s/bin/BIN/g'

  # substitueix en les linies que contenen "elfo"
  sed '/elfo/ s/bin/BIN/g' text

  # des de la línia que contingui "capitol 1" fins la línia que conté "capitol 2"
  sed '/capitol 1/,/capitol 2/ s/bin/BIN/g' text

  # des de la línia que contingui "capitol 2" fins el final
  sed '/capitulo 2/,$ s/bin/BIN/g' text
  ```
  - Podem utilitzar expressions regulars (`PER`) per els patrons de cerca:

  ```bash
  # deixem la línia 3 en blanc
  sed '3 /^.*$//g'

  # Eliminem el primer camp
  echo "Nom:Cognom:Adreça:email" | sed 's/^[^:]*://g'
  Cognom:Adreça:email

  # Reordenem el primer i el segón camp "\1 i \2 es refereixen al parèntesi 1 i 2"
  echo "Nom:Cognom:Adreça:email" | sed -r 's/^([^:]*):([^:]*):/\2:\1:/g'
  Cognom:Nom:Adreça:email

  # Eliminem el segon camp i reordenem els camps 1 i 3
  echo "Nom:Grup:Nota" | sed -r 's/^([^:]*):([^:]*):(.*)$/\3--\1/g'
  Nota--Nom

  # Modifiquem un valor, "&" es refereix al contingut a substituir
  echo "Nom:Grup:Nota" | sed -r 's/Grup/ASI-&/

  # Utilitzem el valor d'una variable
  NOMBRE="Jorge"
  echo "Nom:Grup:Nota" | sed -r 's/Nom/$NOMBRE/g'
  Jorge:Grup:Nota

  # Posar un separados als milers
  echo "45200" | sed -r 's/[0-9]{3}$/.&/g'
  45.200
  ```