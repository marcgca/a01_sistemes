#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Exemples bucle while
# ------------------------------------
ERR_NARGS=1
ERR_NOVALID=2
OK=0
# Validem que hi hagi argument

# -------- exemples processar stdin (entrada estàndard)

cont=1
while read -r line
do			
	echo "$cont: $line" | tr '[a-z]' '[A-Z]'
	cont=$((cont+1))
done
exit 0


# Iterar fins rebre token "FI"
read -r line
while [ $line != "FI" ]
do
  echo "$cont: $line"
	read -r line
done
exit 0

# Mostrar línia a línia la entrada estàndard
cont=1
while read -r line
do
  echo "$cont: $line"
	cont=$((cont+1))
done
exit 0


# -------- exemples de shift i args

# Idem numerant arguments
num=1
while [ -n "$1" ]
do
	echo "$num: $1"
	num=$((num+1))
	shift
done
exit 0

# Iterar per la llista d'arguments mostrant-los un a un.
# Operador shift
while [ -n "$1" ]
do
	echo "$1"
	shift
done
exit 0

# exemple shift
while [ $# -gt 0 ]
do
	echo "$#: $*"
	shift
done
exit 0


# ------ exemples generals while


# Comptador decremental partint de l'argument rebut

MIN=0
nota=$1

while [ $nota -ge $MIN ]
do
	echo "$nota"
	nota=$((nota-1))
done


exit $OK



# Mostrar del 0 al 10 inclosos
MAX=10
num=0

while [ $num -le $MAX ]
do
	echo "$num"
	num=$((num+1))
done


exit $OK
