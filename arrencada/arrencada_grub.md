# Arrencada de la màquina

## Sequència d'arrencada

Quan la màquina s'engega té un procés d'arrencada:

1. POST/BIOS: Detecta el hardware de la màquina i comprova que tot està correcte. Si hi ha algo malament, i si la placa té un speaker, emitirà uns sons (aquests sons es poden mirar al manual i ens indica l'error). A algunes plaques més noves hi ha un petit led on ens dona un còdi d'error.
2. BIOS Setup
   * Seqüència d'arrencada (prioritats): La BIOS té guardat una seqüència d'arrencada on els intenta arrencar mitjançant "fallaback", és a dir, si el primer no funciona, passa al següent.
     * Disc
     * USB
     * PXE
     * ...
3. Si es el disc dur, aquest té l'anomenat MBR (*`Master Boot Record`* ) que conté un bootloader.
   * Abans contenía la taula de particions, on s'indicava la partició activa.
   * Ara conté un bootloader (en cas de Linux, Grub 2)
4. Carrega el grub de la partició que "mana" (des de la qual hem instal·lat el grub)
   * Mostra el menú que tinguem al fitxer de configuració de la partició que mana.
5. Un cop seleccionem (o arrenqui per defecte) la partició dessitjada, aquesta carrega el kernel i l'initramfs.
   * `kernel`: El kernel del sistema, amb la seva versió.
   * `initramfs`:  fs que es carrega en ram per drivers específics.
6. Arrenca el systemd (abans initd), que engega tots els moduls necessaris.
   * **Systemd** és més eficient que **initd** perque arrenca tots els processos necessaris a la vegada (*concurrència*), i aquests ja s'esperen si necessiten que un altre estigui arrencat. Al contrari, **initd** els arrenca en ordre, necessitin o no l'anterior, això vol dir que si un procés tarda molt en carregar tots els que tingui al darrere no arrencaràn fins que aquest no ho hagi fet.
   * Un cop arrencat, i si ha tardat més de lo normal (o simplement volem veure el que ha tardat cada procés al arrencar) podem executar `systemd-analyze blame`.

## GRUB

### Instal·lació de grub

* `grub2-install`

  ```bash
  # Amb /dev/sda li indiquem el disc on volem que s'instal·li el grub, que sol ser el primer disc connectat a la bios en cas de tindre més d'1 a la màquina.
  grub2-install /dev/sda
  #
  # Si volem que s'instal·li a una altre partició, primer hem de montar-la a /mnt i indicar-ho
  mount /dev/sdax /mnt
  grub2-install --boot-directory=/mnt/boot /dev/sda
  ```

* `grub2-mkconfig`

  ```bash
  # Amb grub2-mkconfig llegim els sistemes operatius instal·lats en el sistema i generem un nou menú
  grub2-mkconfig -o /boot/grub2/grub.cfg
  ```

### Edició de grub

Al generar el *grub.cfg* cada entrada consta, entre altres, d'aquests elements necessaris per a la arrencada:

* `insmod` per a carregar els mòduls necessaris.
* `set root` ruta del disc i partició on buscar arxius.
* `linux16` especifica la ruta del kernel a carregar.
* `initrd16` especifica el ramdisk a carregar.

```bash
menuentry 'fedora mati' {
    insmod gzio
    insmod part_msdos
    insmod ext2
    set root='hd0,msdos5'
    linux16 /boot/vmlinuz-5.0.... root=/dev/sda5 
    initrd16 /boot/initrd-5.0.. .img
}

menuentry 'win2' {
    insmod part_msdos
    insmod ntfs # en el caso de partición vfat insmod vfat
    set root=(hd0,2)
    chainloader +1
}

```

### Submenús a grub

* Es poden crear submenús a grub englobant les entrades dessitjades:

  ```bash
  submenu 'nom submenú' {
      menuentry 'nom entrada' {
          ....
      }
      menuentry 'nom entrada' {
          ....
      }
  }
  ```

### Paràmetres adicionals

* Podem afegir opcions per a que el grub, per exemple, arrenqui per defecte una entrada o especificar el temps d'espera per arrencar.
  * `set default=0`: arrenca la primera entrada
  * `set timeout=-1`: especifiquem el temps d'espera, amb **-1** treiem el temps d'espera i ha de ser l'usuari qui seleccioni l'entrada.

### Recuperació de grub

* Ens podem trobar amb dues opcions:

  * Que tot estigui ben instal·lat però, per alguna raó, no troba el *grub.cfg*  i ens aparegui el prompt de grub `#grub>`.

    * Tunejar una entrada existent del grub amb Ctrl+E

    * Escriure una entrada en mode comanda, amb les comandes de cada entrada si sabem on es troba el sistema operatiu que volem arrencar.

    * En mode comanda cridar a algún fitxer *grub.cfg* d'alguna partició existent.

      ```bash
      #grub> configfile (hd0,msdos5)/boot/grub2/grub.cfg
      ```

  * Que el link del mbr a la partició on es troba el grub estigui trencat, ja sigui perque no està bé, perque s'ha esborrat la partició... Apareixerà el prompt del rescue del grub `grub rescue>`.

    * En aquest mode el grub no pot accedir als mòduls, per tant li hem d'especificar on els pot trobar i arrencar en mode normal

      ```bash
      grub rescue> set prefix=(hd0,msdos5)/boot/grub2
      grub rescue> set root=(hd0,msdos5)
      grub rescue> insmod normal
      grub rescue> normal
      ```

      