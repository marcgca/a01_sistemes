#! /bin/bash
#CAPÇALERA
# --------------------
# Mínim 1 argument
if ! [ $# -ge 1 ]
then
  echo "Mínim 1 argument"
  echo "Usage: programa nota"
  exit 1
fi


# Els valors estàn entre 0 i 10
llistaNotes=$*
for nota in $llistaNotes
do
  if [ $nota -ge 0 -a $nota -le 10 ]
  then
		if [ $nota -lt 5 ]
		then
			echo "$nota: Suspés"
		elif [ $nota -lt 7 ]
		then
			echo "$nota: Aprovat"
		elif [ $nota -lt 9 ]
		then
			echo "$nota: Notable"
		else
		 echo "$nota: Excel·lent"
		fi
	else
		echo "$nota: Valor invalid" >> /dev/stderr
		echo "Usage: Valors entre 0 i 10" >> /dev/stderr
	fi
done
exit 0		

