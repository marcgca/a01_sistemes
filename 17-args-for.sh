#! /bin/bash
# @EDT ASIX 2018/2019
# separar en dues llistes args i opcions
#----------------------------------------------------------------------------------------
ERR_NARGS=1
ERR_NOPT=2
OK=0

# 1) Validar n arguments
if [ $# -lt 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: prog.sh [-a, -b, -c, -d, -e] args[...]"
	exit $ERR_NARGS
fi

# 
llistaOpcions=""
llistaArguments=""


for arg in $*
do
	case $arg in
		"-a" | "-b" | "-c" | "-d" | "-e")
				llistaOpcions="$llistaOpcions $arg";;
		*)
				llistaArguments="$llistaArguments $arg";;
	esac
done

echo "opcions: $llistaOpcions"
echo "arguments: $llistaArguments"
exit 0
