#! /bin/bash
# @edt ASIX M01-ASO Curs 2018-2019
# $ llistar-dir-sh dir
#-----------------------------------
# verificar 1 arg, i que és un dir
ERROR_NARG=1
ERROR_NDIR=2
OK=0

# Comprovar que sol hi ha 1 argument
if [ $# -lt 1 ]
then
	echo "ERROR: Mínim un argument"
	echo "Usage: prog.sh dir"
	exit $ERROR_NARG
fi

# Ajuda
if [ "$1"="-h" -o "$1"="--help" ]
then
	echo "Usage: prog.sh dir1 dir2 ..."
	echo "Usage: mínim 1 argument"
	exit $OK
fi

llistaDirs=$*
# comprovar que sigui un direcori
for dir in $llistaDirs	
do
	if ! [ -d $dir ]
	then
		echo "ERROR: Argument invàlid, $dir no és un direcori" >> /dev/stderr
		echo "Usage: prog.sh dir" >> /dev/stderr
	else
  # xixa
	# fa un 'ls' dels direcori rebuts i enumera cada element per línia, mostrant si és un link, un regular file, un directorti o una altre cosa
		echo "---------$dir------------"
		elementsDir=$(ls $dir)
		cont=1
		for element in $elementsDir
		do
			if [ -L $dir/$element ]
			then	
 				echo "$cont: $element és un link"
			elif [ -f $dir/$element ]
			then
				echo "$cont: $element és un regular file"
			elif [ -d $dir/$element ]
			then
				echo "$cont: $element és un directori"
			else
				echo "$cont: $element és una altre cosa"
			fi
 		cont=$((cont+1))
		done
	fi
done

exit $OK
