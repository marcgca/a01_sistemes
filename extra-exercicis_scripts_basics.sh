#! /bin/bash
# @edt ASIX-M01
# Curs 2018-2019
# Exercicis scripts bàsics
# ------------------------------------
ERR_NARGS=1
ERR_NOVALID=2
OK=0

# 10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num de línies.
cont=0
maxLines=$1
while read -r line
do
	cont=$((cont+1))
	echo "$cont: $line"
	if [ $cont -eq $maxLines ]
	then
		exit 0
	fi
done

exit 0




# 9. Fer un programa que rep per stdin nom d'usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.

while read -r user
do
	grep "^$user:" /etc/passwd &> /dev/null
	if [ $? -eq 0 ]
	then
		echo "$user" >> /dev/stdout
	else
		echo "$user" >> /dev/stderr
	fi
done
exit 0


# 8. Fer un programa que rep com a argument noms d'usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.

for user in $*
do
	grep "^$user:" /etc/passwd &> /dev/null
	if [ $? -eq 0 ]
	then
		echo "$user" >> /dev/stdout
	else
		echo "$user" >> /dev/stderr
	fi
done
exit 0

# 7. Processar línia a línia l'entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
while read -r line
do
	numCaracters=$(echo $line | wc -c)
	if [ $numCaracters -gt 60 ]
	then
		echo "$line"
	fi 
done
exit $OK

# 6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la
# setmana genera un error per stderr.
llistaDies=$*
laborables=0
festius=0
for dia in $llistaDies
do
	case $dia in
	"dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres")
		laborables=$((laborables+1));;
	"dissabte" | "diumenge")
		festius=$((festius+1));;
	*)
		echo "$dia: valor invalid" >> /dev/stderr
		echo "Valors vàlids: dies de la setmana" >> /dev/stderr;;
	esac
done
echo "laborables: $laborables"
echo "festius: $festius"
exit $OK


# 5. Mostrar línia a línia l'entrada estàndard, retallant
# només els primers 50 caracters

while read -r line
do
	echo "$line" | cut -c1-50
done
exit 0

# 4. Fer un programa que rep com a arguments números de mes
# (un o més) i indica per a cada mes rebut quants dies té

mesos=$*
for mes in $mesos
do
	case $mes in
	2)
	  echo "$mes: 28 dies";;
	4 | 6 | 9 | 11)
	  echo "$mes: 30 dies";;
	*)
	  echo "$mes: 31 dies";;
	esac
done
exit $OK

# 3. Fer un comptador des de zero fins el valor indicat per l'argument rebut
cont=0
maxim=$1
while [ $cont -le $maxim ]
do
  echo "$cont"
  cont=$((cont+1))
done
exit $OK

# 2. Mostrar els arguments rebuts línia a línia, tot numerànt-los
#llistaArgs= $*
for arg in $*
do
  cont=$((cont+1))
  echo "$cont: $arg"
done
exit $OK


# 1. Mostrar l'entrada estàndard numerant línia a línia

while read -r line
do
  cont=$((cont+1))
  echo "$cont: $line"
  read -r line
done
exit $OK
