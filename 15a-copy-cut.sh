#! /bin/bash
# @EDT ASIX 2018/2019
# Programa que copia uns fitxers a un directori destí
# validar arguments i directori destí
# validar que els arguments són regular files
#----------------------------------------------------------
ERR_NARGS=1
ERR_NODIR=2

# 1) Validar n arguments
if [ $# -lt 2 ]
then
	echo "Error: num args incorrecte"
	echo "usage: prog.sh file[...] dir-destí"
	exit $ERR_NARGS
fi


# 2) obtenim elements
numArgs=$#
dirDesti=$(echo "$*" | cut -d" " -f$numArgs)
llistaFile=$(echo "$*" | cut -d" " -f1-$((numArgs-1)))


# 3) Validar dir-destí
if ! [ -d $dirDesti ]
then
	echo "Error: Directori no vàlid"
	echo "Usage: prog.sh file[...] dir-destí"
	exit $ERR_NODIR
fi

# 4) Verifiquem que els fitxers existeixen i que són regular files

for fitxer in $llistaFile
do
	if ! [ -f $fitxer ]
	then
		echo "Error: $fitxer no és un regular file" >> /dev/stderr
	else
		cp $fitxer $dirDesti
		echo "$fitxer copiat"
	fi
done

exit 0
