#!/bin/bash
#
# Marc Gómez
# isx47797439
# M01
# 13/06/19
# Examen extra UF4
# ------------------------------------------------------
EX_HELP=0
ERR_NARGS=1
ERR_FNOVALID=2
filein=/dev/stdin
# Comprovem arguments
if [ $# -eq 1 ]; then
	if [ $1 == '-h' -o $1 == '--help' ]; then
		echo "Usage: prog.sh [fileIn]"
		exit $EX_HELP
	elif [ -f $1 ]; then
		filein=$1
	else
		echo "Error: fitxer $1 no vàlid"
		echo "Usage: prog.sh [fileIn]"
		exit $ERR_FNOVALID
	fi
elif [ $# -gt 1 ]; then
	echo "Error: num args invalid"
	echo "Usage: prog.sh [fileIn]"
	exit $ERR_NARGS
fi

# Iterem per entrada
errors=0
ok=0

while read -r line
do
	echo $line
	# Normalitzem
	dades=$(echo $line | tr -s "[:blank:]" " ")
	# Recollim dades
	host=$(echo $dades | cut -d" " -f1)
	schema=$(echo $dades | cut -d" " -f2)
	llista_fitxers=$(echo $dades | cut -d" " -f3-)
	# Iterem per cada fitxer
	for file in $llista_fitxers
	do
		echo $file | grep ":" &> /dev/null
		# Si hi ha user
		if [ $? -eq 0 ]; then
			# Recollim l'user i passwd
			user=$(echo $file | cut -d":" -f1)
			passwd=$(echo $file | cut -d":" -f2)
			fitxer=$(echo $file | cut -d":" -f3)
			# Descarreguem
			wget --user=$user --passwd=$passwd $schema://$host/$fitxer
			# Comprovem si ha anat bé
			if [ $? -eq 0 ]; then
				echo "fitxer $fitxer copiat correctament" >> /var/log/downget.log
				ok=$((ok+1))
			else			
				echo "wget $user:$passwd $schema://$host/$fitxer" >> /dev/stderr
				errors=$((errors+1))
			fi
		# Si no hi ha user
		else
			wget $schema://$host/$file
			if [ $? -eq 0 ]; then
				echo "fitxer $file copiat correctament" >> /var/log/downget.log
				ok=$((ok+1))
			else
				echo "wget $schema://$host/$file" >> /dev/stderr
				errors=$((errors+1))
			fi
		fi
	done
done < $filein

total=$((errors+ok))

echo "Files Downloaded"
echo "Total:$total, Ok:$ok, Err:$errors" | sed 's/^/\t/g'

exit $errors
