﻿# Administració de sistemes informàtics en Xarxa.
# Escola del treball de Barcelona.  2018-2019.
# M01-ISO Implantació de sistemes operatius.
# Examen 16
# Serveis de xarxa i accés remot
# HISX1
# 31/05/2019
# UF3-NF4-A4-14 Serveis de xarxa
# UF3-NF4-A4-15 Administració remota
# GNU/Linux Administració
# Cal fer-ho tot!
# 
# 1. (5 punts)
# Fes un programa que rep un o més arguments corresponents a noms de host o adreçes IP. 
# Per a cada un d’ells el programa verifica la connectivitat amb ping 
# (li pasem -c3 perquè faci únicament 3 intents). Si hi ha connectivitat 
# es mostra un missatge per stdout, si no n’hi ha e smostra per stderr. El programa retorna 
# un codi corresponent al número d’errors de connectivitat.
# Comprovació d'arguments
if [ $# -eq 0 ]; then
    echo "Error: no arguments" >> /dev/stderr
    echo "Usage: prog.sh host[...]"
    exit 1
fi 

# Guardem els hosts/ip
n_errors=0
llista_hosts=$*
for host in $llista_hosts
do
    ping -c3 $host &> /dev/null
    if [ $? -eq 0 ]; then
        echo "Connectivitat a $host OK"
    else
        echo "Error al connectar a $host" >> /dev/stderr
        n_errors=$((n_errors+1))
    fi
done
exit $n_errors

# 2. (5 punts)
# Fes un programa que processa un fitxer rebut com a argument  o bé l’entrada 
# estàndard si no es passa cap argument. Cada línia del fitxer o de l’endrada conté 
# un nom de servei del sistema, una unit tipus service. Verificar si el servei 
# està actiu (no cal fer res si ho està), si no ho està engegar-lo tot indicant-ho per stdout. 
# Si en activar-lo es produeix un error indicar-ho per stderr. El programa retorna un status del 
# número total de serveis que han generat error en intentar engegar-los.

# Comprovació arguments
file=/dev/stdin
if [ $# -eq 1 ]; then
    if [ -f $1 ]; then
        file=$1
    else
        echo "Error $1 no vàlid" >> /dev/stderr
        echo "Usage: prog.sh [file]" >> /dev/stderr
        exit 1
    fi
fi

n_errors=0
while read -r servei
do
    systemctl is-active $servei &> /dev/null
    if [ $? -ne 0 ]; then
        systemctl start $servei &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Servei $servei engegat"
        else
            echo "Error engegant el servei $servei" >> /dev/stderr
            n_errors=$((n_errors+1))
        fi
    fi
done < $file
exit $n_errors