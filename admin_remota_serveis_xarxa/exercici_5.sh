# !/bin/bash
# Marc Gómez
# isx47797439
# hisx1 2018-2019
# Exercici del exemple05_2010
# ----------------------------------------
#NARGS_INCORRECTE = 1
#ERR_NFIT = 2
# Comprovem els arguments, si no hi ha agafem l'entrada estàndard
fitxer=/dev/stdin
if [ $# -eq 1 ]; then
	fitxer=$1
elif [ $# -ge 2 ]; then
	echo "Num args incorrecte"
	echo "Usage: prog [fitxer]"
	exit 1
fi

# Comprovem que el fitxer existeix
if ! [ -e $fitxer ]; then
	echo "El fitxer $fitxer no existeix"
	exit 2
fi

n_errors=0
dadesUser=''
# Recorrem el fitxer
while read -r line
do
	# normalitzem les dades i guardem els camps
	dades=$(echo $line | tr -s "[:blank:]" " ")
	type=$(echo $dades | cut -d" " -f1)
	server=$(echo $dades | cut -d" " -f2)
	recurs=$(echo $dades | cut -d" " -f3)
	
	# Comprovem si hi ha últim camp per guardar-lo
	echo $dades | egrep "^([^ ]* ){3}[^ ]*$" > /dev/null
	if [ $? -eq 0 ]; then
		dadesUser=$(echo $dades | cut -d" " -f4)
		user=$(echo $dadesUser | cut -d: -f1)
		password=$(echo $dadesUser | cut -d: -f2)
	fi

	# Per a nfs
	if [ $type == 'nfs' ]; then
		options=" $server:$recurs "
	elif [ $type == 'smb' ]; then
		# Comprovem si no ens han donat les credencials d'autenticació
		if [ -z $dadesUser ]; then
			dadesUser='-o guest'
		else
			dadesUser="-o user=$user,pass=$password"
		fi
		options=" $dadesUser //$server/$recurs "
		type='cifs'
	fi

	# Comprovem si es pot montar i mostrem el resultat
	mount -t $type $options /mnt &> /dev/null
	if [ $? -eq 0 ]; then
		umount /mnt
		echo "OK: $type $recurs" >> /dev/stdout
	else
		echo "Error: $type $server $recurs" >> /dev/stderr
		echo "Error: $type $server $recurs" >> log_errors.txt
		n_errors=$((n_errors+1))
	fi

done < $fitxer

exit $n_errors














