#!/bin/bash
#
# Marc Gómez
# isx47797439
# HISX 2018-2019
# M01
# 31/05/19
# Examen final UF4 



function primer(){ 
# Validem argument
file=/dev/stdin
if [ $# -eq 1 ]; then
	if [ $1 == '-h' -o $1 == '--help' ]; then
		echo "Usage: prog.sh [fileIn]"
		exit 0
	else
		file=$1
	fi
elif [ $# -gt 1 ]; then
	echo "Error: nº args incorrecte"
	echo "Usage: prog.sh [fileIn]"
	exit 1
fi

# Iterem per contingut

while read -r line
do
	# Normalitzem les dades
	dades=$(echo $line | tr -s "[:blank:]" ":")
	schema=$(echo $dades | cut -d":" -f2)
	host=$(echo $dades | cut -d":" -f1)
	fitxers=$(echo $dades | cut -d":" -f3)
	
	
	echo 	$schema
	echo $line | sed -s 's/^(*)$/\t\1/g'

done < $file	

return 0
}

function primer_rep(){
# Validem argument
file=/dev/stdin
if [ $# -eq 1 ]; then
	if [ $1 == '-h' -o $1 == '--help' ]; then
		echo "Usage: prog.sh [fileIn]"
		return 0
	elif [ -f $1 ]; then
		file=$1
	else
		echo "Fitxer $1 no vàlid" >> /dev/stderr
		return 1
	fi
elif [ $# -gt 1 ]; then
	echo "Error: nº args incorrecte" >> /dev/stderr
	return 2
fi

# Si és entrada estàndard, recollim les dades a un fitxer
if [ $# -eq 0 ]; then
	while read -r line
	do
		echo $line >> fitxer-$$.log
	done
	file=fitxer-$$.log
fi
# Recollim els diferents schemes
llista_schemes=$(cat $file | tr -s "[:blank:]" ":" | cut -d":" -f2 | sort -u)

# Iterem per tipus
for schema in $llista_schemes
do
	entrades=$(cat $file | tr -s "[:blank:]" ":" | cut -d":" -f2 | egrep "^$schema$" | wc -l)
	# Mostrem per cada tipus
	echo "$schema:$entrades"
	cat $file | tr -s "[:blank:]" " " | egrep "^[^ ]* $schema " | cut -d" " -f1,3- | sort | sed 's/^/\t/g'
done

if [ $# -eq 0 ]; then
	rm $file
fi

return 0
}

function segon(){
#Iterem entrades
n_errors=0
while read -r line
do
	# Normalitzem
	dades=$(echo $line | tr -s "[:blank:]" " ")
	# Agafem les dades
	host=$(echo $dades | cut -d" " -f1)
	userpasswd=$(echo $dades | cut -d" " -f2)
	fitxers=$(echo $dades | cut -d" " -f3-)
	# Agafem l'user i password
	user=$(echo $userpasswd | cut -d":" -f1)
	password=$(echo $userpasswd | cut -d":" -f2)
	# Copiem els fitxers
	sshpass -p $password scp $fitxers $user@$host:/home/$user
	# Comprovem que hagi anat correctament
	if [ $? -eq 0 ]; then
		echo "$fitxers copiats" >> copy.log
	else
		n_errors=$((n_errors+1))
	fi

done

return $n_errors
}



function tercer(){
# Comprovació d'arguments
if [ $# -ne 1 ]; then
		echo "Error: n args incorrecte"
		echo "Usage: prog.sh fitxer"
		exit 1
elif [ $1 == '-h' -o $1 == '--help' ]; then
		echo "Usage: prog.sh fitxer"
		exit 0
elif [ -f $1 ]; then
		file=$1
else
		echo "Fitxer incorrecte"
		echo "usage: prog.sh fitxer"
		exit 2
fi

n_errors=0
while read -r line
do
	# Normalitzem
	dades=$(echo $line | tr -s "[:blank:]" " ")
 	# Agafem les dades
	host=$(echo $dades | cut -d" " -f1)
	userpasswd=$(echo $dades | cut -d" " -f2)
	fitxers=$(echo $dades | cut -d" " -f3-)
	# Agafem l'user i password
	user=$(echo $userpasswd | cut -d":" -f1)
	password=$(echo $userpasswd | cut -d":" -f2)
	# Iterem per cada grup de fitxers
	for fitxer in $fitxers
	do
		fileorigen=$(echo $fitxer | cut -d":" -f1)
		filedesti=$(echo $fitxer | cut -d":" -f2)
		# Copeim l'arxiu
		sshpass -p $password scp $fileorigen $user@$host:$filedesti
		# Comprovem que hagi anat correctament
		if [ $? -eq 0 ]; then
			echo "$fitxer copiat" >> /dev/stdout
		else
			echo "$fitxer no copiat" >> errors.log
			n_errors=$((n_errors+1))
		fi
	done

done < $file
return $n_errors
}

function tercer_dos(){
# Comprovació d'arguments
if [ $# -ne 1 ]; then
	echo "Error: num args incorrecte" >> /dev/stderr
	echo "Usage: prog.sh fitxer" >> /dev/stderr
	exit 1
else
	if [ $1 == '-h' -o $1 == '--help' ]; then
		echo "Usage: prog.sh fitxer"
		exit 0
	elif [ -f $1 ]; then
		file=$1
	else
		echo "Error: fitxer $1 no vàlid" >> /dev/stderr
		echo "Usage: prog.sh fitxer"
		exit 2
	fi
fi

n_errors=0
while read -r line
do
	# Normalitzem
	dades=$(echo $line | tr -s "[:blank:]" " ")
	# Agafem les dades
	host=$(echo $dades | cut -d" " -f1)
	userpasswd=$(echo $dades | cut -d" " -f2)
	fitxers=$(echo $dades | cut -d" " -f3-)
	# Agafem l'user i la password
	user=$(echo $userpasswd | cut -d":" -f1)
	passwd=$(echo $userpasswd | cut -d":" -f2)
	# Iterem per cada fitxer
	for fitxer in $fitxers
	do
		fileorigen=$(echo $fitxer | cut -d":" -f1)
		filedesti=$(echo $fitxer | cut -d":" -f2)
		# Copiem l'arxiu
		sshpass -p $passwd scp $fileorigen $user@$host:$filedesti
		if [ $? -eq 0 ]; then
			echo "Fitxer $fitxer copiat OK"
		else
			echo "Error fitxer $fitxer no copiat" >> errors.log
			n_errors=$((n_errors+1))
		fi				
	done
done

return $n_errors
}
