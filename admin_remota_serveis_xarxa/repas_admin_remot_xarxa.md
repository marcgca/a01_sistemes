# Administració remota i serveix en xarxa

## Serveis de xarxa

* Serveis de xarxa:

  * http, ftp, mtftp
  * xinetd
  * nfs/samba
  * ssh


* Hi han dos modes/tipus en els que ens podem trobar els serveis:
  * `Standalone`: El servei s'engega i es queda executant-se esperant a alguna petició.
  * `Xinetd`: **xinetd** és un administrador d'altres serveis, els serveis estàn apagats i **xinetd** els gestiona. Quan rep una petició pel servei (al port assignat a aquell servei), l'engega i quan termina el torna a apagar. 

### Interfícies i ports

A un mateix host podem tindre diferents interfícies de xarxa, ja siguin físiques o virtuals, cada una d'elles té de 0 a 65535 ports disponibles.

* 0-1023 -> ports privilegiats
* 1024-? -> reservats/dinàmics

##### Socket 

Un socket es denomina com la connexió entre la `ip:port origen` i `ip:port destí` .

#### Examinar tràfic i ports

* Podem veure quins ports tenim oberts amb `nmap localhost`:

  ```bash
  marc@portatil:~$ nmap localhost
  
  Starting Nmap 7.60 ( https://nmap.org ) at 2019-06-12 17:52 CEST
  Nmap scan report for localhost (127.0.0.1)
  Host is up (0.000069s latency).
  Not shown: 998 closed ports
  PORT     STATE SERVICE
  631/tcp  open  ipp
  5432/tcp open  postgresql
  ```

* Si el que volem és examinar el tràfic ho podem fer amb `netstat` o `ss` (actualització de **netstat**) i una de les opcions:

  ```bash
  -x # connexions unix
  -a # totes
  -l # listening
  -n # numero de port
  -p # processos
  -u # connexions udp
  -t # connexions tcp
  -c # repetició de l'ordre cada segon
  ```

* Les connexions *tcp* són **STREAM** i les connexions *udp* són **DGRAM** (**datagrama**)

  ```bash
  unix  3      [ ]         STREAM     CONNECTED     16377    /var/run/dbus/system_bus_socket
  unix  2      [ ]         DGRAM                    36797 
  ```

  ```bash
  # Tipus ip
  0.0.0.0:* # IPv4
  [::]:* # IPv6
  ```

#### Parts d'un servei de xarxa

Els serveis de xarxa tenen diferents parts definides:

* Executable: arxiu binari que executa el servei
  * `bin`: executable.
  * `sbin`: executable amb permisos de root.
* Configuració: normalment definida a `/etc/servei/servei.conf` o si és molt extensa es subdivideix en un directori `/etc/servei/servei.d/`.
* Publicació: directori on es publica els arxius que es vulguin mostrar, per exemple a serveis com httpd, ftp... En el cas del httpd és `/var/www/html`.

#### Tipus de serveis

##### Standalone

Tots els serveis standalone tenen un arxiu `.service`, en finalitzar s'apaguen.

##### Xinetd

Els diferents serveis de **xinetd** es troben a `/etc/xinetd.d/ `:

* echo: port **7**
* daytime: port **13**
* chargen: port **19**

#### Uri/schema

El uri fa referència a **Uniform Resource Identifier**, és a dir que identifica el tipus de recurs.

El schema fa referència al tipus de recurs i s'especifica amb:

```bash
schema://host:port/ruta
# Per exemple, per http
http://host:port/ruta
# Tots tenen la mateixa estructura, excepte el de file, que té una  / més
file:///adreça/fitxer
```

### FTP

FTP (**File Transfer Protocol**) és un protocol de tranferència de fitxers en **TEXT PLÀ**.

* Treballa als ports 20 (**dades**) i 21 (**control**) amb el protocol **TCP**.
* Té 2 modes de treball:
  * `Actiu`: Quan l'executem des del servidor cap a una altre màquina.
  * `Passiu`: Quan es cridat per una altre màquina, mentrestant està a la espera.
* Té 2 modes d'usuari:
  * `Usuari anònim`: l'usuari que s'hi connecta és anònim. Usualment només pot descarregar fitxers, però no pujar-los ni veure l'arrel de l'equip.
  * `Usuari identificat`: l'usuari s'identifica com a usuari del **servidor** o d'una llista d'usuaris. Pot pujar i navegar si té els propis permisos al sistema i al servei.
* El directori de publicació és `/var/ftp`.
* Connexions:
  * Quan un client anònim es connecta ho fa al directori de publicació `/var/ftp/pub`.
  * Si és un usuari registrat, es connecta al seu home.

### wget

**wget** serveix per a descarregar arxius.

```bash
# Descarreguem el fitxer d'una url
wget ftp://ghandi/pub/config.tgz
# Especificant usuari, contrasenya i servidor
wget ftp://user10:passwd10@server/fitxer.txt
# Especificant usuari i servidor, mentres que la password és interactiva
wget ftp://user10@server/fitxer.txt --ask-password
```

### Telnet

Telnet és una eina per a connexions remotes, però també és en **text plà**, per lo que no es gaire segur si algú ens està esnifant la connexió. És recomable fer servir **ssh**.

* Treballa al port **23** .

### DHCP

DHCP és un protocol d'assignació d'adreçes IP, el client és `dhclient`.

* Treballa als ports **67** i **68**.

  ```bash
  # Allibera la ip actual
  dhclient -r # -r = release
  # Demana una IP veient les passes
  dhclient -v enp2s0
  ```

### DNS

DNS és un protocol de resolució de noms de domini, és a dir, relaciona una ip amb el seu nom.

* Treballa al port **53**

* A l'arxiu `/etc/resolv.conf` trobem a quins dns busca la nostra màquina quan no sap qui és un host/ip.

  ```bash
  # Generated by NetworkManager
  search informatica.escoladeltreball.org
  nameserver 192.168.0.10
  nameserver 10.1.1.200
  nameserver 208.67.222.222
  nameserver 8.8.8.8
  ```

* El FQDN és el nom complet qualificat de domini, acava amb un **.** al final.

* Els DNS principals del món es poden trobar amb `host -t any`.

### Ports de serveis

Al fitxer `/etc/services` es pot trobar tots els ports associats a cada servei amb una breu descripció del mateix.

### SMTP

SMTP (**Simple Mail Transfer Protocol**) és el protocol de correu.

* Treballa al port **25** amb dos variants segons el tipus de correu que fagi servir:
  * `POP`: Port **110**
  * IMAP: Port **143**.

### NFS

NFS (**Network File System**) és un protocol de sistema de fitxers en línia, permet compartir carpetes per a que es puguin montar a una altre màquina de forma remota.

* Treballa al port **2049**.

* El fitxer de directoris a exportar es troba a `/etc/exports`. 

* L'estructura general és `directori_a_compartir domini/ip amb opcions del mount`.

  ```bash
  # sample /etc/exports file
  /               master(rw) trusty(rw,no_root_squash)
  /projects       proj*.local.domain(rw)
  /usr            *.local.domain(ro) @trusted(rw)
  /home/joe       pc001(rw,all_squash,anonuid=150,anongid=100)
  /pub            *(ro,insecure,all_squash)
  /srv/www        -sync,rw server @trusted @external(ro)
  /foo            2001:db8:9:e54::/64(rw) 192.0.2.0/24(rw)
  /build          buildhost[0-9].local.domain(rw)
  ```

* Amb l'ordre `exportfs` exportem les direccions del fitxer **exports**.

  ```bash
  # Exportar totes les direccions
  exportfs -a
  # verbose
  -v
  # resetejar exportacions
  -r
  ```

* Com a client, podem montar una carpeta compartida nfs amb **mount**:

  ```bash
  mount -t nfs maquina:recurs desti
  mount -t nfs j09:/projects /mnt
  ```

  * Si al montar no especifiquem l'usuari, ens identificarà com a usuari *nobody* 

  * Podem veure quines màquines han montat el directori d'exportació d'una màquina concreta.

    ```bash
    showmount j16
    ```

### SAMBA

Samba és un protocol de transferencia de dades basat en els protocols de Windows *cifs*, *smb*, per poguer compartir directoris entre Linux i Windows. Això vol dir que si el servidor és un Linux, amb aquests protocols, les altres màquines Windows el veuen com un Windows.

* Samba té 2 serveis:

  ```bash
  smb # compartició de fitxers / impresores
  nmb # resolucions de netbios (win)
  ```

* Es poden localitzar les xarxes compartides a la nostra xarxa local amb:

  ```bash
  smbtree -L # mostra totes les xarxes compartides a la xarxa local
  smbclient -L //localhost # mostra les xarxes que estem compartint nosaltres. També podem especificar una altre màquina si volguèssim.
  ```

* L'arxiu d'exportacions de samba es troba a `/etc/samba/smb.conf`, on es troba una secció global i després una de *shares* que són individuals.

  ```bash
  [manuals]
          comment = Compartint els man # descripció
          path = /usr/share/man # ruta del que volem compartir
          read only = yes # només lectura
          guest ok = yes # admet guest
          browseable = yes # s'anuncia el recurs al broadcast
  
  ```

  * Altres opcions:

    ```bash
    valid users = marta # sol la marta es pot loguejar
    read list = marta # marta no pot escriure, sol llegir
    
    ```

  * Un cop fetes les modificacions pertinents es recomenable testejar la sintaxi del fitxer amb `testparm`.

* A no ser que en el fitxer de configuració indiquem el contrari, s'accedirà a samba amb un usuari existent des d'on es pugui loguejar al servidor. Aquest usuari tindrà una contrasenya de samba que es pot afegir amb `smbpasswd -a user`. Tots aquests usuaris es podràn llistar i estaràn guardats a la base de dades de samba `/var/lib/samba/*.tdb`.

  ```bash
  pdbedit -L # Llistem els comptes de samba
  ```

* Per poguer accedir a un recurs compartit es fa amb l'ordre `smbclient`.

  ```bash
  smbclient //server/recurs 
  smbclient //server/recurs -U user
  ```

  * Al conectar-nos a un recuers estarem "enjaulats", és a dir que no podrem accedir per sobre del directori arrel del recurs.

* Al igual que amb wget, samba té la seva comanda que funciona igual:

  ```bash
  smbget smb://server/recurs/fitxer
  ```

* Per montar un recurs de samba al sistema:

  ```bash
  mount -t cifs //server/recurs /mnt -o guest
  ```

  * Si no especifiquem l'opció *guest* es montarà amb l'usuari actual.

### SSH

Ssh (secure shell) és un protocol d'accés remot segur, és a dir, les dades són encriptades entre les màquines.

* Treballa al port **22**. Tot i que té més comandes, com scp, sftp i interactua com a aquestes ordres, **segueix** utilitzant el port 22.

* La configuració del client es guarda a `/etc/ssh/ssh_config`, i la del servidor a `/etc/ssh/sshd_config`.

* Identifiquem el paquet ssh i totes les seves ordres:

  ```bash
  rpm -qa | grep ssh 
  rpm -ql openssh-clients | grep bin
  ```

* Quan accedim a un altre host identifica les màquines amb `fingerprints`, és a dir, una clau pública única que es desa al home de l'usuari `~/.ssh/known_hosts`. A la primera connexió amb un host no guardat, ens advertirà i preguntarà si hi confiem. Si troba que és diferent a la que tenim guardada ens avisarà i tancarà la connexió.

  * Podem generar les nostres pròpies claus amb `ssh-keygen`.

* Connexions bàsiques:

  ```bash
  ssh user@server # Connexió remota interactiva
  ssh user@server "date; id" # Execució de comandes al host remot
  ssh -X user@server gedit # Execució de gedit amb X11 locals
  ```

* També podem copiar arxius via ssh, ja que no deixa de ser un fluxe:

  ```bash
  # Copia remota
  ls httpd.log | ssh root@j25 "cat - > /tmp/copia.txt"
  # Mitjançant scp
  scp httpd.log root@j25:/tmp/copia.txt
  ```

* Podem veure les claus públiques que emmagatzema un host amb `ssh-keyscan host`.

* També podem fer backups entre hosts:

  ```bash
  # De local a remot
  tar czf - fitxer | ssh host@server "cat - > ./fitxer.tar.gz"
  tar czf - directori/fitxer | ssh host@server "tar xzf - -C /directori/nou"
  # De remot a local
  ssh host@server "tar czf - id2" | > id.tar.gz
  ssh host@server "tar czf - dns-server" | tar xzf -
  
  ssh host@server "gzip -c fitxer.txt" > file.gz
  ```

  

