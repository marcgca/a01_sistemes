# !/bin/bash
# Marc Gómez
# isx47797439
# hisx1 2018-2019
# Exercici del exemple01_2011
# ----------------------------------------
# Comprovem els arguments
if [ $# -eq 1 ]; then
	if [ $1 == '--help' ]; then
		echo "Usage: prog.sh [--usa] data[...]"
		exit 0
	elif [ $1 == '--usa' ]; then
		echo "Error no data"
		echo "Usage: prog.sh [--usa] data[...]"
		exit 1
	fi
elif [ $# -ge 2 ]; then
	if [ $1 = '--usa' ]; then
		type='usa'
		shift
	else
		type='eu'
	fi
else
	echo "Error no data"
	echo "Usage: prog.sh [--usa] data[...]"
fi

# Recorrem els arguments
n_dates=0
dates_correc=0
dates_err=0
format="^[0-1][0-9]-[0-1][0-9]-[1-2][0-9]$"
if [ $type = 'usa' ]; then
	format="^[1-2][0-9]{3}-[0-1][0-9]-[0-1][0-9]$"
fi

for date in $*
do
	echo $date | egrep $format >> /dev/null
	if [ $? -eq 0 ]; then
		dates_correc=$((dates_correc+1))
		n_dates=$((n_dates+1))
	else
		echo $date >> /dev/stderr
		dates_err=$((dates_err+1))
		n_dates=$((n_dates+1))			
	fi

done

echo "($n_dates) dates: ($dates_correc)Ok, ($dates_err)Error" >> /dev/stdout
exit 0









